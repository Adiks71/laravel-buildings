<header class="main-header">
    <!-- <img class="title" src="img/vector logo.svg"  width="470px" hight="40px" alt="logo"> -->
    <img class="logo" src="/img/vector logo.svg"  width="470px" hight="40px" alt="logo">
    <nav>
        <ul class="main-header-nav">
            <li class="nav-item one">
                <span>Современные здания</span>
            </li>
            <li class="nav-item">
                <span>Старинные здания</span>
            </li>
            <li class="nav-item">
                <span>Последние проекты</span>
            <li class="nav-item two">
                <a href=" {{route('frontend.architecture.news')}}">Архитектуртые новости</a>
            </li>
        </ul>
    </nav>
</header>