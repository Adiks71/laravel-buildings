<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Архитектурный блог 3</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/main3.css" />
</head>
<body>
@include('frontend.includes.header')
<main>
    <section class="breadcrumbs">
        <div class="emphasize"></div>
        <div class="home">
            <a class="G" href="{{ route("frontend.architecture") }}">Главная</a>
            <span>\</span>
            <a class="A" href="#">Архитектуртые новости</a>
        </div>
    </section>
    <section class="news-arhitect">
        <div class="unisual_bildings">
            <div class="red">
                <span class="red_button">
                    Архитектурные новости
                </span>
            </div>
        </div>
    </section>
    <section class="avesome">
        <span class="unisual">
            здание «доминион» потрясающего архитектора захи хадид
        </span>
        <div class="news_info">
            <div class="avthor">
            <img src="/img/Vector.svg" width="33px" height="33px" alt="avthor">
            <div class=" avthor_text">
                <div class=" avthor_text_gteen">
                    <p >Автор</p>
                </div>
                <div class=" avthor_text_red">
                    <p>Админ</p>
                </div>
            </div>
        </div>
        <div class="clock">
            <img src="/img/Group 365.svg"  width="33px" height="33px" alt="clock">
            <div class="clock_text">
                <div class=avthor_text_gteen>
                    <p>Время на прочтение</p>
                </div>
                <div class="clock_text_red">
                    <p>3 мин</p>
                </div>
            </div>
        </div>
        <div class="data">
            <img src="/img/Group 366.svg" width="33px" height="33px" alt="data">
            <div class="data_text">
                <div class="avthor_text_gteen">
                    <p>Опубликовано</p>
                </div>
                <div class="data_text_red">
                    <p>2019-11-12</p>
                </div>
            </div>
        </div>
        </div>
        <div class="emphasize-2"></div>
    </section>
    <section>
        <div class="bioptic">
            <p class="text-1">
                Заха Мохаммад Хади́д – ирако-британский архитектор и дизайнер арабского происхождения, представительница деконструктивизма.
                В 2004 году стала первой в истории женщиной, награждённой Притцкеровской премией.
            </p>
            <p>
                Также Заха Хадид была посмертно удостоена наградой Blueprint Award for Architecture
                2016 года, за которую проголосовало сообщество Blueprint.
            </p>
            <p>
                «У Blueprint были давние отношения с Zaha, поскольку она ввела в эксплуатацию свою
                самую первую структуру в Великобритании - павильон еще в 1995 году», - сказал редактор
                Blueprint Magazine Джонни Такер. «Несмотря на то, что она оставила замечательное
                наследие зданий, как кардифф, мне лично очень жаль, что ей так и не удалось построить
                свой оперный театр в столице Уэльса. Это - огромная потеря для мира архитектуры 21-го
                века ».
            </p>
            <p>
                В некрологе в марте редактор журнала Blueprint Герберт Райт написал: «Какая
                у нее необычайная сила. Она была сильным, ощутимым гением дизайна, который оставил
                свой след на нашей планете – элементы из лучшего будущего, здесь и сейчас ».
            </p>
            <p>
                Строительство бизнес-центра началось в 2008 году, однако из-за
                экономического кризиса работы на некоторое время были приостановлены.
                При участии российского архитектора Николая Лютомского
                проект был
                доработан и адаптирован для московских условий того периода, и в 2012 году
            </p>
            <p> строительство возобновилось. Открытие здания состоялось 25 сентября
                2015 года.
                Бизнес-центр представляет собой семиэтажное здание общей площадью 21,4
                тысячи м², из которых 9,7 тысяч м² отведено под офисные помещения. Также
                в здании располагается парковка на 251 место.
            </p>
            <p>
                В первоначальном проекте присутствовали 20-метровые консоли, бесшовные
                меняющие цвет панели и элементы, создающие ощущение полёта. Однако в
                процессе адаптации проекта под российские реалии было принято решение
                сократить «сдвиги» до 8 метров и отказаться от некоторых элементов.
            </p>
            <p>
                Проектирование бизнес-центра осуществлялось по методологии параметрической
                архитектуры — с применением компьютерной графики на специально созданном
                программном обеспечении. Данная технология использовалась и при создании
                других крупных проектов Захи Хадид, например Центра водных видов спорта
                в
                Англии, Культурного центра Гейдара Алиева в Азербайджане, Национального
                музея искусств MAXXI века в Италии и других. По словам заместителя
                генерального директора Zaha Hadid Architects Патрика Шумахера, в основу
                проекта легли принципы русского авангарда, которые потом были развиты в
                современной архитектуре.
            </p>
        </div>
    </section>
    <section class="read">
        <h3>С этим читают</h3>
        <div class="type-bildings">
            <a href="#"><div class="bloc-read">
                <img src="/img/956f08a01735a684ee9fe95aa27973b0.jpg" width="145px" height="132px" alt="956f08">
                <div class="popular">
                    <div class="bloc-read-big">Популярные здания в стиле деко...</div>
                    <div class="bloc-read-tinc">АВГУСТ 2019</div>
                </div>
            </div>
            </a>
            <a href="#"><div class="bloc-read">
                <img src="/img/Дом_печати_-_Казань.jpg" width="145px" height="132px" alt="Казань">
                <div class="popular">
                    <div class="bloc-read-big">Самое изогнутое здание</div>
                    <div class="bloc-read-tinc">СЕНТЯБРЬ 2019</div>
                </div>
            </div>
            </a>
            <a href="#"><div class="bloc-read">
                <img src="/img/0QJyK9n7Uyw.jpg" width="145px" height="132px" alt="0QJyK9n7Uyw">
                <div class="popular">
                    <div class="bloc-read-big">Стеклянная революция</div>
                    <div class="bloc-read-tinc">ДЕКАБРЬ2019</div>
                </div>
            </div>
            </a>
        </div>
    </section>
</main>
@include('frontend.includes.footer')
</body>
</html>