<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> Архитектурный блог</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel="stylesheet">
</head>
<body>
@include('frontend.includes.header')
<main>
    <section class="actual">
        <div class="big_image">
            <img src="img/Bitmap.jpg" width="779.33px" height="489.16px" alt="Bitmap">
            <h1>Самые актуальные архитуктурные новости</h1>
            <button class="looc">смотреть</button>
        </div>
        <div class="moda">
            <div class="year_2019">
                <img src="img/Image_01.jpg" width="373.09px" height="240.43px" alt="Image_01">
                <button class="looc right_button">смотреть</button>
            </div>
            <div class="structure">
                <img src="img/Image_02.jpg" width="373.09px" height="240.43px" alt="Image_02">
                <button class="looc right_button">смотреть</button>
            </div>
        </div>

    </section>
    <section class="modern_buildings">
        <div class="quarto">
            <div class="ofice_buildings">
                <div>
                    <span class="red_button">Современные здания</span>
                </div>
                <img src="img/sasha-yudaev-FOYsU4uQqqM-unsplash 1.jpg" width="404px" height="623" alt="ofice_buildings">
                <div class="ofice">
                    <span>Офистые здания</span>
                    <div class="line"></div>
                </div>
            </div>
            <div class="trion">
                <div class="long">
                    <div class="quarters_one">
                        <img src="img/yann-allegre-v1jO6pN45_4-unsplash 1.jpg" width="332px" height="292px" alt="quarters">
                    </div>
                    <div class="quarters_two">
                        <img src="img/jude-beck-sPuSvj2g_C8-unsplash 1.jpg" width="" height="" alt="two">
                    </div>
                </div>
                <div class="quarters_three">
                    <img src="img/mark-boss-Xf2kJBZ2aJA-unsplash 1.jpg" width="" height="" alt="three">
                </div>
                <div class="g">
                    <span>Жилые помещения</span>
                    <div class="line"></div>
                </div>

                <div class="pointer">
                    <div class="top">
                        <span>наверх</span>
                    </div>
                    <div class="arroy">
                        <img src="img/Group 189.svg" width="27px" height="27.1px" alt="arroy">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mailing">
        <div class="newsletter">
            <div class="subscript">
                <span>Подпишитесь на нашу e-mail рассылку</span>
            </div>
            <div class="mail_container">
                <div class="e-mail">
                    <input type="input" value="Введите ваш e-mail">
                </div>
                <button class="subscript_button">
                    подписться
                </button>
            </div>
        </div>
    </section>
    <section class="modern_buildings">
        <div class="quarto">
            <div class="ofice_buildings">
                <div>
                    <span class="red_button">Старинные здания</span>
                </div>
                <img src="img/krishna-moorthy-d-SPvxAdwoUzw-unsplash 1.jpg" width="579px" height="386px" alt="krishna">
                <div class="ofice">
                    <span>Религиозные строения</span>
                    <div class="line"></div>
                </div>
            </div>
            <div class="trion">
                <div class="trion-img">
                    <div class="long bich">
                        <div class="quarters_one">
                            <img src="img/ashim-d-silva-_4HhZOUEwmw-unsplash 1.jpg" width="303px" height="200px" alt="ashim">
                        </div>
                        <div class="quarters_two khee">
                            <img src="img/yeo-khee-hyZ8ln856Ag-unsplash 1.jpg" width="303" height="133" alt="yeo-khee">
                        </div>
                    </div>
                    <div class="quarters_three gielda">
                        <img src="img/filip-gielda-VPavA7BBxK0-unsplash 1.jpg" width="217" height="346" alt="filip-gielda">
                    </div>
                </div>
                <div class="g">
                    <span>Туристические здания</span>
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="news-arhitect">
        <div class="unisual_bildings">
            <div class="red">
                <span class="red_button">Архитектурные новости</span>
            </div>
            <div class="unisual">
                <h1>Самые не обычные здания </h1>
                <h1 class="text">во всем мире</h1>
            </div>

            <button class="looc arhitect">смотреть</button>
            <div class="ceys-one">
                <img src="img/Group 7.1.png" width="" height="" alt="ceys">
            </div>
            <div class="ceys-two">
                <img src="img/Group 7.1-1.png" width="" height="" alt="ceys">
            </div>
        </div>
    </section>
    <section class="mailing before-footer">
        <div class="newsletter">
            <div class="subscript">
                <span>Подпишитесь на нашу e-mail рассылку</span>
            </div>
            <div class="mail_container">
                <div class="e-mail">
                    <input type="input" value="Введите ваш e-mail">
                </div>
                <button class="subscript_button">
                    подписться
                </button>
            </div>
        </div>
    </section>
</main>
@include('frontend.includes.footer')
</body>
</html>