
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> Архитектурный блог 2</title>
    <link rel="stylesheet" type="text/css" href="css/main2.css" />
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
</head>
<body>
@include('frontend.includes.header')
<main>
    <section class="news-arhitect">
        <div class="emphasize"></div>
        <div class="home">
            <a class="G" href="{{ route("frontend.architecture") }}">Главная</a>
            <span>\</span>
            <a class="A" href="#">Архитектуртые новости</a>
        </div>
        <div class="unisual_bildings">
            <div class="red">
	             		<span
                                class="red_button">Архитектурные новости
	             	    </span>
                <span class="unisual">самые не обычные здания во всем мире
	             		</span>
                <span class="namber">Номер10/50</span>
            </div>
        </div>
        <div class="topic">
            @foreach($news['data'] as $newsItem)
            <article class="dominion">
                <a href="#">
                    <div class="dominion-1" style="background-image: url('{{ $newsItem['url'] }}');">
                        <div class="dominion-link" href="#">перейти</div>
                    </div>
                    <h1 class="dominion-text">{{ $newsItem['text'] }}</h1>
                </a>
            </article>
            @endforeach
            {{--<article class="dominion">--}}
                {{--<a href="#">--}}
                    {{--<div class="dominion-1" style="background: url(img/mitchell-luo-Rzvqw-4EQ00-unsplash3.jpg);">--}}
                        {{--<div class="dominion-link" href="#">перейти</div>--}}
                    {{--</div>--}}
                    {{--<h1 class="dominion-text">жилое помещение «с чешуей»</h1>--}}
                {{--</a>--}}
            {{--</article>--}}
            {{--<article class="dominion">--}}
                {{--<a href="#">--}}
                    {{--<div class="dominion-1" style="background: url(img/Mask_Group.jpg)";>--}}
                        {{--<div class="dominion-link" href="#">перейти</div>--}}
                    {{--</div>--}}
                    {{--<h1 class="dominion-text">деконструктивизм во всей своей красе</h1>--}}
                {{--</a>--}}
            {{--</article>--}}
        </div>
        <div class="scroll">
            @for($i = 1; $i <= $news['last_page']; $i++)
                @if($news['current_page'] === $news['last_page'] and $i === $news['last_page'])
                <a class="hyphen"></a>
                @endif
                <a class="scroll-1 @if($i === $news['current_page'])red @endif" href="http://arch2.loc/architecture_news?page={{ $i }}">{{ $i }}</a>
                @if($i === $news['current_page'] and $i !== $news['last_page'])
                <a class="hyphen"></a>
                @endif
            @endfor
            {{--<a class="scroll-2" href="#">2</a>--}}
            {{--<a class="scroll-3" href="#">3</a>--}}
            {{--<a class="scroll-4" href="#">4</a>--}}
            {{--<a class="scroll-5" href="#">5</a>--}}
            {{--<a class="scroll-6" href="#">6</a>--}}
        </div>
    </section>
    <section class="mailing">
        <div class="newsletter">
            <div class="subscript">
                <span>Подпишитесь на нашу e-mail рассылку</span>
            </div>
            <div class="mail_container">
                <div class="e-mail">
                    <input type="input" value="Введите ваш e-mail">
                </div>
                <button class="subscript_button">
                    подписться
                </button>
            </div>
        </div>
    </section>
</main>
@include('frontend.includes.footer')
</main>
</body>
</html>

