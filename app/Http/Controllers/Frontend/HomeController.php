<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ArchitectureNews;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.index');
    }

    public function architecture()
    {
        return view('frontend.architecture');
    }

    public function architecture_news()
    {
        $news = ArchitectureNews::query()
            ->select(['url', 'text'])
            ->orderBy('id', 'asc')
            ->paginate(3)
//            ->get()
            ->toArray();

//        dd($news);

        return view('frontend.architecture_news', compact('news'));
    }

    public function get_news()
    {
        return view('frontend.get_news');
    }
}

